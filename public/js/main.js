overlay = null;
full_image = null;

function enlarge(node) {
	show(overlay);
	full_image = document.createElement("img");
	full_image.id = "full_image";
	overlay.appendChild(full_image)
	path = node.src.replace("thumb_", "");
	full_image.src = path;
	full_image.alt = node.alt;
	window.getSelection().removeAllRanges();
}

function show(node) {
	node.style.display = "inline";
}

function hide(node) {
	node.style.display = "none";
	if(full_image)
		overlay.removeChild(full_image)

	full_image = null;
}

function keydown(event) {
	if(event.which == 27 || event.which == 81)
		hide(overlay);
}

window.onload = function() {
	images = document.getElementsByClassName("enlarge");
	for(i = 0; i < images.length; i++)
		images[i].addEventListener('click', function() { enlarge(this); });

	overlay = document.getElementById("overlay");
	overlay.addEventListener('click', function() { hide(this); });

	full_image = document.getElementById("full_image");

	document.addEventListener("keydown", keydown, false);
}
